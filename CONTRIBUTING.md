This is a KiCAD project thus merging can be tricky  
Coordinate with project engineers before starting any changes  

## Schematic

Changes in manufacturer part numbers field, footprints and designators can be merged most of the times  
Any part change, change inplacement, wire placement etc cannot be merged at this point

One exception is Hierarchical sheets provided that:

* A single person is working on a Hierarchical sheet
* Annotation is using sheet number
* Global nets are respected
* Changes in Hierarchical labels are coordinated

## PCB

At this point any merging on PCB files must be avoided
